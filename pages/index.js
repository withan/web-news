import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <div className="container">
      <div className="row">
        <div className="col col-6">A</div>
        <div className="col col-6">B</div>
      </div>
    </div>
  );
}
